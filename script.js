//  let trainer = {
//  	name: "Pika",
//  	age: 2,
//  	pokemon: [],
//  	friends: {
//  		value:[],
//  	}
//  }




let trainer = {};

trainer.name = `ash ketchum`
trainer.age = 10
trainer.pokemon = [`pikachu`, `charizard`]
trainer.friends = {
	kanto: [`Brock`, `Misty`],
	hoenn: [`Max`, `Macy`]
}

trainer.talk = () => {
	console.log(`pikachu i choose you`)
}
console.log(trainer)
//6
console.log(trainer.name)

console.log(trainer["age"])
//7
console.log(trainer.talk())

function Pokemon(name, lvl) {
	this.name = name;
	this.level = lvl;
	this.health = lvl*2
	this.attack = lvl
	this.intro = function(opponent){
		const{name}=opponent// object destructuring
		console.log(`Hi im ${this.name} and this is ${opponent.name}`)
	};
	this.tackle = (target) =>{
		//console.log(target)
		target.health - this.attack
	};
	this.faint = (target) =>{
		console.log(`${target} has fainted`);
	}
}

let Pikachu = new Pokemon("Pikachu", 12)
console.log(Pikachu)

let Geodude = new Pokemon("Geodude", 8)
console.log(Geodude)

let Mewto = new Pokemon("Mewto", 12)
console.log(Mewto)

//10

Geodude.tackle(Pikachu)
